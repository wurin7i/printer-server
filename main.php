<?php

require_once 'vendor/autoload.php';

$argv = $_SERVER['argv'];
$data = $argv[1] ?? '';
$buffer = str_replace('escpos://', '', $data);

if (!empty($buffer)) {
    $printBuffer = new PrinterServer\PrintBuffer();
    $printBuffer->run($buffer);
}

