<?php

namespace PrinterServer;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\PrintConnector;
use Printer\Bridge;
use Printer\BufferTransport;

class PrintBuffer
{
    public function run(string $buffer)
    {
        try {
            $connector = $this->getPrinterConnector();
            $transport = new BufferTransport();
            $bridge = new Bridge($transport);
            $bridge->connect($connector);
            $bridge->print($buffer);
        }
        catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    protected function getPrinterConnector(): PrintConnector
    {
        $configFileName = null;
        $connector = new FilePrintConnector($configFileName ?? '/dev/usb/lp0');

        return $connector;
    }
}